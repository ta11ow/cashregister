﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cash_Register
{
    class Special
    {
        private byte id;
        private uint? count = null;
        private decimal price = 0m;

        public uint? Count
        {
            get => this.count;
            set => this.count = value;
        }
        public decimal Price
        {
            get => this.price;
            set => this.price = value;
        }

        public Special(byte id)
        {
            this.id = id;
        }
    }
}
