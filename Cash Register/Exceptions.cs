﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cash_Register
{
    public class InsufficientStockException : Exception
    {
        public InsufficientStockException()
        {
            MessageBox.Show("InsufficientStockException!", "Insufficient Stock!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public InsufficientStockException(string message)
            : base(message)
        {
            MessageBox.Show("InsufficientStockException: " + message, "Insufficient Stock!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public InsufficientStockException(string message, Exception inner)
            : base(message, inner)
        {
            MessageBox.Show("InsufficientStockException: " + message, "Insufficient Stock!", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
