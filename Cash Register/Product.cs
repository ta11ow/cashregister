﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cash_Register
{
    class Product
    {
        private byte id;
        public byte ID
        {
            get => id;
        }
        public int Stock = 0;

        private decimal unitprice = 0m;
        public decimal UnitPrice
        {
            get => unitprice;
            set => unitprice = value;
        }

        private string name = "";
        public string Name
        {
            get => name;
            set => name = value;
        }

        public Product(byte id, decimal unitprice, string name, int stock)
        {
            this.id = id;
            this.unitprice = unitprice;
            this.name = name;
            this.Stock = stock;
        }
    }
}
