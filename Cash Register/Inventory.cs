﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cash_Register
{
    class Inventory
    {
        private List<Product> items = new List<Product>();
        public void AddProduct(Product item)
        {
            if (!this.items.Contains(item))
            {
                this.items.Add(item);
                this.items.OrderBy(x => x.ID);
            }
        }
        public void RemoveProduct(int id, int amount)
        {
            int location = this.items.FindIndex(x => x.ID == id);
            if (this.items[location].Stock < amount)
                throw new InsufficientStockException("Could not find sufficient items in inventory list.\nRemaining items: " + this.items[location].Stock);
            else
                this.items[location].Stock -= amount;
        }
        public Product[] Items
        {
            get => this.items.ToArray();
        }
    }
}
